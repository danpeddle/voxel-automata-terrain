#version 330

out vec4 fragColor;

uniform sampler2D texture;
uniform vec2 texOffset;

in vec4 vertColor;
in vec4 vertTexCoord;

void main() {
  vec2 p = vertTexCoord.st/texOffset.st + 0.5;
  vec2 i = floor(p);
  vec2 f = p - i;
  //f = f*f*f*f*(f*(f*(-20*f + 70 ) - 84) + 35); // these make it too square
  //f = f*f*f*(f*(f*6.0-15.0)+10.0);
  f = f*f*(3 - 2*f);
  p = i+f;
  p = (p - 0.5)*texOffset.st;
  
  fragColor = texture2D(texture, p);
}